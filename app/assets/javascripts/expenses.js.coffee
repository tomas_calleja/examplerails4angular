root = global ? window

angular.module("expenses", ["ngResource"]).factory "Expense", ['$resource', ($resource) ->
  Expense = $resource("/expenses/:id",
    id: "@id"
  ,
    update:
      method: "PUT"

    destroy:
      method: "DELETE"
  )
  Expense::destroy = (cb) ->
    Expense.remove
      id: @id
    , cb

  Expense
]
root.angular = angular
