
root = global ? window

ExpensesIndexCtrl = ($scope, Expense) ->
  $scope.expenses = Expense.query()

  $scope.destroy = ->
    if confirm("Are you sure?")
      original = @expense
      @expense.destroy ->
        $scope.expenses = _.without($scope.expenses, original)
        
ExpensesIndexCtrl.$inject = ['$scope', 'Expense'];

ExpensesCreateCtrl = ($scope, $location, Expense) ->
  $scope.save = ->
    Expense.save $scope.expense, (expense) ->
      $location.path "/expenses/#{expense.id}/edit"

ExpensesCreateCtrl.$inject = ['$scope', '$location', 'Expense'];

ExpensesShowCtrl = ($scope, $location, $routeParams, Expense) ->
  Expense.get
    id: $routeParams.id
  , (expense) ->
    @original = expense
    $scope.expense = new Expense(@original)

  $scope.destroy = ->
    if confirm("Are you sure?")
      $scope.expense.destroy ->
        $location.path "/expenses"

ExpensesShowCtrl.$inject = ['$scope', '$location', '$routeParams', 'Expense'];

ExpensesEditCtrl = ($scope, $location, $routeParams, Expense) ->
  Expense.get
    id: $routeParams.id
  , (expense) ->
    @original = expense
    $scope.expense = new Expense(@original)

  $scope.isClean = ->
    console.log "[ExpensesEditCtrl, $scope.isClean]"
    angular.equals @original, $scope.expense

  $scope.destroy = ->
    if confirm("Are you sure?")
      $scope.expense.destroy ->
        $location.path "/expenses"

  $scope.save = ->
    Expense.update $scope.expense, (expense) ->
      $location.path "/expenses"

ExpensesEditCtrl.$inject = ['$scope', '$location', '$routeParams', 'Expense'];

# exports
root.ExpensesIndexCtrl  = ExpensesIndexCtrl
root.ExpensesCreateCtrl = ExpensesCreateCtrl
root.ExpensesShowCtrl   = ExpensesShowCtrl
root.ExpensesEditCtrl   = ExpensesEditCtrl 